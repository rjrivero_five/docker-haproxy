consul    = "$CONSUL_NODE"
log_level = "warn"
retry     = "10s"
max_stale = "10m"
pid_file  = "/home/haproxy/consul-template.pid"

template {
    source      = "/etc/consul-template/haproxy.ctmpl"
    destination = "/etc/haproxy/haproxy.cfg"
    command     = "chmod 0644 /etc/haproxy/haproxy.cfg && /usr/sbin/haproxy -f /etc/haproxy/haproxy.cfg -p /home/haproxy/haproxy.pid -D -sf $(cat /home/haproxy/haproxy.pid)"
}
