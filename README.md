HAProxy container with Consul auto-configuration
================================================

Lighweight container (based on [baseimage-docker](https://github.com/phusion/baseimage-docker)) prividing an [HAProxy](http://www.haproxy.org/) server with auto-configuration based on [consul-template](https://github.com/hashicorp/consul-template)

To build the container:

```
git clone https://bitbucket.org/five_corp/docker-haproxy
cd docker-haproxy

# Consul-template version is hard-coded in the Dockerfile and downloaded
# as a pre-compiled pack. Edit Dockerfile to upgrade version.
docker build -t docker-haproxy .
```

To run:

```
docker run --rm -e CONSUL_NODE=<HOST>:<PORT> -p 8443:8443 -p 8000:8000 -p 5858:5858 --name proxy docker-haproxy
```

The container exposes  the **HTTP and HTTP ports, 8000 and 8443** respectively,
by default. The SSL certificate can be auto-generated at first container run,
or can be provided as a volume (-v /path/to/pem:**/opt/haproxy/pem**). The file
must be readable by user haproxy (currently uid:gid = **100:1000**)

The container now also exposes **Port 5858**, intented for the websocket of [Node Inspector](https://github.com/node-inspector/node-inspector). The port is also protected by SSL.

Environment variables
---------------------

The container accepts the following environment variables:

  - CONSUL_NODE: *ADDRESS*:*PORT* of the Consul node to which the server should attach.
  - CONSUL_KEY: Path of the Consul K/V store to be used for configuration keys.
  - LOG_HOST: URL of the syslog server to use, in the form *ADDRESS*:*PORT*.
  - STATS_SERVICE: Service name assigned to the haproxy stats. The
    haproxy server is configured to enable the statistics page on an internal
    port. The stats service is treated like a regular service by the name of
    *$STATS_SERVICE*: if the corresponding key is found under
    *$CONSUL_KEY/services*, then the service is published.

Configuring services
--------------------

Services are configured under the Consul prefix specified by the environment variable **CONSUL_KEY** (*haproxy* by default). Services accept the following keys:

  - $CONSUL_KEY/services/*$SERVICE_NAME*/domain: Host / domain name of the service (may include the port, e.g.: **my.domain.com**)
    - The match is performed against the **beginning**, to avoid problems when the port number is explicitly specified (as in *www.somedomain.com:8000*). As a side effect, you can match on subdomain (e.g. **stats.**)
  - $CONSUL_KEY/services/*$SERVICE_NAME*/url_reg: Regular expression matching the path e.g.: **/myapp**)
  - $CONSUL_KEY/services/*$SERVICE_NAME*/debug: If this key exists, connections to port **5858** in the server are forwarded to the host port specified by this key. I.E. if your Node Inspector's web socket is listening at port 5858, set **debug=5858**.
  - $CONSUL_KEY/services/*$SERVICE_NAME*/skip_ssl: If this key exists, any attempt to connect to the service URL via http will be permitted instead of redirected to https.
  - $CONSUL_KEY/services/*$SERVICE_NAME*/auth/*$GROUP*: If you specify a group name in this key, then the service is password protected, and only users belonging to the given group can access it. Notice these are not operating system groups, but haproxy groups; see below.

If the service name matches the value given by the environment variable
*$STATS_SERVICE*, then all requests to the given domain name or url will be
automagically forwarded to the stats page of the haproxy server.

Configuring authentication
--------------------------

This container supports basic authentication. You can define a set of users and
groups under the **$CONSUL_KEY/auth** key:

  - $CONSUL_KEY/auth/groups/*$GROUP*: Group name.
  - $CONSUL_KEY/auth/users/*$USER*/groups: Comma-separated list of groups to which the user belongs.
groups/*$GROUP*: Group name.
  - $CONSUL_KEY/auth/users/*$USER*/password: mkpasswd-generated password for the user.

To generate a password compatible with haproxy, use the **mkpasswd** utility.
For instance:

```
# Add a space before the command if you don't want it recorded in bash-history
 mkpasswd -m sha-512 
```
